import { LightningElement, api, track } from 'lwc';

export default class TaskCard extends LightningElement {
    @api task;
    openModal=false;

    handleDragStart()
    {
        console.log('taskCard :: start drag => ' + this.task.taskId);
        this.dispatchEvent(new CustomEvent('itemdrag', {detail: this.task}));
    }

    handleOpenModal()
    {
        console.log('taskCard :: handleOpenModal');
        this.openModal = true;
    }

    handleDrop(event)
    {
        console.log('taskCard :: item drop');

        event.preventDefault();
        this.dispatchEvent(new CustomEvent('itemdrop', {detail: this.task.taskId}));
    }

    handleSubmitModal(event)
    {
        let taskFields = event.detail.fields;
        taskFields.taskId = this.task.taskId;

        const submitModalEvent = new CustomEvent('submitmodal', {detail: event.detail.fields});
        this.dispatchEvent(submitModalEvent);
    }

    handleCancelModal()
    {
        console.log('taskCard :: handleCancelModal');
        this.openModal = false;
    }

    handleDeleteTask()
    {
        console.log('taskCard :: handleDeleteTask');                
        this.dispatchEvent(new CustomEvent('deletetask', {detail: this.task.taskId}));
    }

    handleDoNothing(event)
    {
        event.stopPropagation(); // Prevent parent's onclick div from propogating
    }

    handleDragOver(event)
    {
        event.preventDefault();
    }
}