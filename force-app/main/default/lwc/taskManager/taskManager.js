import { LightningElement, wire } from 'lwc';
import getUserToDoLists from '@salesforce/apex/TaskManagerController.getUserToDoLists';
import moveTodoTask from '@salesforce/apex/TaskManagerController.moveTodoTask';
import { deleteRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';


export default class TaskManager extends LightningElement {
    taskLists = [];
    error;
    openModal = false;

    _recordResponse;

    @wire(getUserToDoLists, {source: 'placeholder'})
    wiredTaskLists(response)
    {
        console.log('taskManager :: wiredTaskLists');

        this._recordResponse = response;
        if(response.data) {
            console.log("Size retrieved from server: ", response.data.length);
            console.log(response.data);
            this.taskLists = response.data;
        } else if (response.error) {
            console.log("Error fetching user todo lists!");
            console.log(response.error);
            this.error = response.error;
        }
    }

    draggingTask;
    listModalMode;
    selectedList;

    connectedCallback()
    {
        console.log('Initializing TaskManager component . . .');
    }

    handleItemDrop(event)
    {
        let droppedPositionId = event.detail;
        console.log('taskManager :: handleItemDrop');        
        console.log('dragging task: ' + this.draggingTask.taskId + ' | dropped position Id: ' + droppedPositionId);
        
        // Update local view
        let targetPosition = this.findListPositionByTaskId(droppedPositionId);

        // Update positions in database
        moveTodoTask({movedTaskId: this.draggingTask.taskId, targetListId: targetPosition.taskListId,
            targetPosition: targetPosition.index}).then(() => {
                
            console.log('Successfully moved task.');
            refreshApex(this._recordResponse);
        }).catch(error => {
            console.log('Error moving task!');
            console.log(error);
        });
    }

    handleListItemDrag(event)
    {        
        console.log('taskManager :: handle list item drag ' + event.detail.taskId);
        this.draggingTask = event.detail;
    }

    handleSubmitModal(event)
    {
        console.log('taskManager :: handleSubmitModal');
        refreshApex(this._recordResponse);
    }

    handleCreateTask(event)
    {
        console.log('taskManager :: handleCreateTask');
        refreshApex(this._recordResponse);
    }

    handleModalSubmit(event)
    {
        console.log('taskManager :: handleModalSubmit');

        refreshApex(this._recordResponse);
        this.resetModalParams();
    }

    handleCreateListModal()
    {
        console.log('taskManager :: handleCreateListModal');

        this.listModalMode = 'create';
        this.selectedList = null;
        this.openModal = true;
    }

    handleCloseListModal(event)
    {
        console.log('taskManager :: handleCloseListModal');
        this.resetModalParams()
    }

    handleDeleteTask(event)
    {
        console.log('taskManager :: handleDeleteTask');
        // let originListId = event.detail.taskListId;
        let deletedTaskId = event.detail.taskId;

        deleteRecord(deletedTaskId).then(() => {
            console.log('Successfully deleted task: ' + deletedTaskId);
            refreshApex(this._recordResponse);
        }).catch(error => {
            console.log('Error deleting task!');
            console.log(error);
        });      
    }
    
    handleDeleteList(event)
    {
        console.log('taskManager :: handleDeleteList');

        let deletedListId = event.detail;

        deleteRecord(deletedListId).then(() => {
            console.log('Successfully deleted list: ' + deletedListId);
            refreshApex(this._recordResponse);
        }).catch(error => {
            console.log('Error deleting list!');
            console.log(error);
        });
    }

    resetModalParams()
    {
        this.listModalMode = null;
        this.selectedList = null;
        this.openModal = false;
    }

    findListPositionByTaskId(targetTaskId)
    {
        // Find position to move task to
        for(let i=0; i < this.taskLists.length; i++)
        {
            for(let j=0; j < this.taskLists[i].taskList.length; j++)
            {
                if(this.taskLists[i].taskList[j].taskId == targetTaskId)
                {
                    var targetPosition = {
                        taskListId: this.taskLists[i].taskListId,
                        index: j
                    };

                    return targetPosition;
                }
            }
        }
    }
}