import { LightningElement, api, track } from 'lwc';

export default class TaskList extends LightningElement {
    @api listName;
    @api taskList = [];
    @api taskListId

    openModal;
    placeholder;

    connectedCallback()
    {
        this.openModal = false;
    }

    handleItemDrag(event)
    {
        console.log('taskList :: item drag');        
        this.dispatchEvent(new CustomEvent('listitemdrag', {detail: event.detail}));
    }

    handleDragOver(event)
    {
        event.preventDefault();
    }

    handleItemDrop(event)
    {
        console.log('taskList :: item drop');        
        this.dispatchEvent(new CustomEvent('itemdrop', {detail: event.detail}));
    }

    handleSubmitModal(event)
    {
        console.log('taskList :: handleSubmitModal');

        let taskFields = event.detail;
        taskFields.taskListId = this.taskListId;

        this.dispatchEvent(new CustomEvent('submitmodal', {detail: taskFields}));
    }

    handleCreateModal(event)
    {
        console.log('taskCard :: handleCreateModal');
        console.log(event.detail);

        this.handleCloseModal();        
        this.dispatchEvent(new CustomEvent('createtask', {detail: event.detail}));
    }

    handleDeleteTask(event)
    {
        console.log('taskList :: handleDeleteTask');

        let updatedDetail = {
            taskId: event.detail,
            taskListId: this.taskListId }

        this.dispatchEvent(new CustomEvent('deletetask', {detail: updatedDetail}));
        
    }

    handleDeleteList(event)
    {
        console.log('taskList :: handleDeleteList');
        this.dispatchEvent(new CustomEvent('deletelist', {detail: this.taskListId}));
    }

    handleOpenModal()
    {
        console.log('taskList :: handleOpenModal');
        this.openModal = true;
    }

    handleCloseModal()
    {
        console.log('taskList :: handleCloseModal');
        this.openModal = false;
    }

}