import { LightningElement, api, track } from 'lwc';
import TO_DO_TASK_OBJECT from '@salesforce/schema/To_Do_Task__c';
import NAME_FIELD from '@salesforce/schema/To_Do_Task__c.Name';
import DESCRIPTION_FIELD from '@salesforce/schema/To_Do_Task__c.Description__c';
import TO_DO_LIST from '@salesforce/schema/To_Do_Task__c.To_Do_List__c';

export default class TaskModal extends LightningElement {

    @api task;
    @api mode;
    @api taskListId

    objectApiName = TO_DO_TASK_OBJECT;
    createFields;
    editFields = [NAME_FIELD, DESCRIPTION_FIELD, TO_DO_LIST];
    title;

    get isEdit()
    {
        return this.mode === 'edit';
    }

    get isCreate()
    {
        return this.mode === 'create';
    }

    connectedCallback()
    {
        // Set title
        this.title = (this.task === undefined) ? 'New Task' : this.task.taskName;

        // Set todo list Id if create mode, with default value of clicked task list
        if(this.isCreate) {
            this.createFields = [
                { name: NAME_FIELD, value: ''},
                { name: DESCRIPTION_FIELD, value: ''},
                { name: TO_DO_LIST, value: this.taskListId}
            ];
        }

        // console.log('createFields:');
        // console.log(this.createFields);
        // console.log('editFields:');
        // console.log(this.editFields);
    }

    handleReset()
    {
        console.log('taskModal :: handleReset()');

        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }

        this.handleClose()
    }

    handleClose()
    {
        console.log('taskModal :: handleClose()');
        
        const cancelModalEvent = new CustomEvent('cancelmodal');
        this.dispatchEvent(cancelModalEvent);
    }
}