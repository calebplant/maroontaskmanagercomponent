import { LightningElement, api, track } from 'lwc';
import TASK_LIST_OBJECT from '@salesforce/schema/To_Do_List__c';

export default class TaskListModal extends LightningElement {

    @api list;
    
    objectApiName = TASK_LIST_OBJECT;
    mode = 'create';
    title;

    // TODO: implement edit mode? (@api mode)

    get isEdit()
    {
        return this.mode === 'edit';
    }

    get isCreate()
    {
        return this.mode === 'create';
    }

    get listId()
    {
        return this.list === null ? '' : this.list.listId;
    }

    connectedCallback()
    {
        this.title = (this.mode === 'create') ? 'New Todo List' : this.list.listName;
    }

    handleReset()
    {
        console.log('taskListModal :: handleReset');
        
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        };

        this.handleClose();
    }

    handleClose()
    {
        console.log('taskListModal :: handleClose');
        this.dispatchEvent(new CustomEvent('cancelmodal'));
    }

    handleSuccess(event)
    {
        console.log('taskListModal :: handleSuccess');        
        this.dispatchEvent(new CustomEvent('createlist', {detail: event.detail}));
    }
}