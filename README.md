# Maroon Task Manager

## Overview

A lightning web component mimicking the functionality of [Trello](https://trello.com/ "Trello's Homepage"). Users can easily add and manage task lists, creating and dragging individual tasks between them as needed. Tasks can also be rearranged within lists to prioritize importance.

## In Action

A picture is worth a thousand words, so I'll let these gifs summarize its current functionality.

### Moving Existing Tasks

![Drag and drop/delete tasks](https://media.giphy.com/media/PlsZE7Ruq3altyBJDk/giphy.gif)

### Creating Tasks

![Create tasks](https://media.giphy.com/media/mA7hp81aKpU1CIWL9n/giphy.gif)

### Managing Task Lists

![Create lists](https://media.giphy.com/media/Z9z2cpbORhQVspaeix/giphy.gif)

## Motivation

Going into this project, I had an extremely basic understanding of lightning web components. Therefore, I have 4 objectives:

1. Learn how to build a basic LWC from the ground up
1. Gain familiarity with basic JavaScript as it is used on client-side controllers
1. Learn how to test both client and server side controllers for LWCs
1. Understand how to manage event propogation between components

## Immediate Todo's

* Split up modal forms into edit/delete
* Separate utility JS functions into utility class
* Rename custom events for clarity
* Finish writing JS tests
* Refactor (starting with event propogation)
* Make it prettier
